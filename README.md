<div align="center">

<h1>qpm (Qemu Passthrough Manager)</h1>
<h3> The Qemu Passthrough Manager is a lightweight toolkit for managing KVM virtual machines on a linux operating system. There are plenty of tools out there which do the same but the most are outdated, ugly or absolutely oversized for simple use cases. Furthermore this toolkit wants to help you doing PCI passthrough to speed up your VMs.</h3>

---

<p align="center">
<img alt="Logo banner" src="./documentation/images/banner.png"/>

<a href="https://gitlab.com/qemum/qpm/commits/master"><img src="https://gitlab.com/qemum/qpm/badges/master/pipeline.svg" alt="pipeline status" /></a>
<a href="https://goreportcard.com/report/gitlab.com/qemum/qpm"><img src="https://goreportcard.com/badge/gitlab.com/qemum/qpm" alt="code quality" /></a>

<br/>

|QPM|QPG|
|:-------:|:------:|
|<a href="https://gitlab.com/qemum/qpm/commits/master"><img src="https://gitlab.com/qemum/qpm/badges/master/coverage.svg?job=test_qpm" alt="QPM test coverage" /></a>|<a href="https://gitlab.com/qemum/qpm/commits/master"><img src="https://gitlab.com/qemum/qpm/badges/master/coverage.svg?job=test_qpg" alt="QPG test coverage" /></a>|

<br/>

[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

<h3>"A man is happy so long as he chooses to be happy."</h3>
<h3>"When you're cold, don't expect sympathy from someone who's warm."</h3>
<h3>"To stand up for truth is nothing. For truth, you must sit in jail."</h3>

***Aleksandr Isayevich Solzhenitsyn***

---

</div>

## Getting Started

1. [Prepare your host system for Qemu manager](./documentation/preparations.md)
1. [Create your own VM configuration](./documentation/config.md)
1. [Using the Qemu manager dashboard](./documentation/dashboard_manual.md)

Or you can use the [Quick Start Example](./documentation/quickstart.md).

## Project Structure

This project contains the following tools which results in QEMU-M as a whole.

- `qpm`: Qemu & passthrough Manager
- `qpg`: virtual machine guest agent for multiple OSes

The directory structure will be shortly explained.

- `cmd`: contains the GoLang code for the executables of this project
  - `qpm`: The GoLang source code for the Qemu manager
  - `qpg`: The GoLang source code for the Qemu agent
- `shared`: contains shared GoLang source code which can be used by all tools within this project
- `html`: contains the website templates for the qpm dashboard
  - `images`: contains images and logos for the dashboard of the Qemu manager

## I'd like to aid this sweet little penguin

This project is completely financially independent. I do **not** accept any donations. If you want to help, create a pull request/an issue and suggest some changes/share some ideas. Please only share ideas which enhance this project in small steps, do **not** provide suggestions like: 

- `Why not using Proxmox and create a library/extensions for it?`
- `Why using GoLang? Java/C#/C++/C/... can do it in a more efficient way`
- `There are simpler possibilities for what you want to do`

Please just accept the approach of this project and **help** me improving the code, make it more readable + fixing bugs.

**Nevertheless you can follow the instructions in the [development guide](./documentation/development.md) to help me improving this toolkit.**