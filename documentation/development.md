<div align="center">

# Development

I won't tell you how you have to write code. This won't be the purpose of Open Source community assistance. But I expect a few rules to be followed.

---

<p align="center">
<img alt="Logo banner" src="./images/penguin_development.png" style="width: 50%"/>

---

</div>

TODO structure description

## Development Tools

The project is made with Google's Go. So you can choose your favourite IDE for developing. You only need Go and the dependency packages of this project. For all lazy developer who use linux (and X11) I made a Docker Container with embedded Visual Studio Code and GoLang (preconfigured). So it's a Docker Container with GUI.

### Using the Docker VSCode IDE (works only with Linux and X11)

1. Clone this repository
1. Download and install docker
1. Add your user to the `docker` group
1. Change to the `tools/vscode` directory of this repository
1. Run the `./run.sh` command

If you do not trust this way you can build the docker image on your own. The corresponding `Dockerfile` is also in the `tools/vscode` directory.

## Error handling

Every single method which can throw an exception is forced to embed this exception into a `Qpmerror` (or `Qpgerror` for QPG) with an error code which describes exactly what happened. In addition in GoLang we have the possibility to return multiple values. For this project every method with possible exceptions is going to return a `Qpmerror` object as secondary/third/... return parameter.

The following example will help you understanding the approach.

``` Go
func increment(val int) int {
    return val + 1
}
```

This `increment` method is fine. No possible exceptions so it's not needed to add any error return parameter.

``` Go
func validateValueDivisionByThree(val int) (bool, *Qpmerror) {
    rest := val % 3
    switch (rest) {
    case 1:
        return false, &Qpmerror{Code: VALIDATION_DIVISION_REST1}
    case 2:
        return false, &Qpmerror{Code: VALIDATION_DIVISION_REST2}
    default:
        return true, nil
    }
}
```

This method defines a custom `Qpmerror` with a specific error code which is defined as const in `errorhandler.go`. If no error occurs just return nil.

``` Go
func Readconfig(configPath string) (*Qpmconfig, *Qpmerror) {
	config, err := ioutil.ReadFile(configPath)
	if err != nil {
		return nil, &Qpmerror{Code: CONFIG_FILE_NOTREADABLE, InnerError: err.Error()}
	}
	res := Qpmconfig{}
	json.Unmarshal(config, &res)
	return &res, validateconfig(&res)
}
```

In this case the `ReadFile` function can return a GoLang standard error object which is not redirectable for us. So we need to wrap the error message by creating a custom `Qpmerror`, defining an error code for this special case and declaring the system error message as inner error. Furthermore the error code has to be described in the [error code overview](errordescription.md) for the user.

You can say that this way is way to heavy. I guess you are right. But I think it's the most comfortable way for the user to get exact error codes for the interactions to make the troubleshooting as easy as possible.

1. Return a custom Qpmerror in your method
1. Give some codes to your custom error and add it as const to `errorhandler.go`
    ``` Go
    const (
        // UnknownError is an unknown error.
        UnknownError = 1
        // ConfigFileNotReadable means that the config file does not exist or the permissions are not set correctly.
        ConfigFileNotReadable = 2
    )
    ```
1. Add an error message to your error code in `errorhandler.go`
    ``` Go
    switch err.Code {
	case ConfigFileNotReadable:
		return fmt.Sprintf("Could not find the config file path. %s", err.InnerError)
	default:
		return fmt.Sprintf("Unknown error. %s", err.InnerError)
	}
    ```
1. Add an error description for the user to the [error code overview](errordescription.md) for your error code

    | Error Code | Stringified Name | Description |
    |:----------:|:----------------:|:-----------:|
    |0|SUCCESS|No error occurred|
    |1|UNKNOWN_ERROR|An unknown error occurred. Please check the inner error message for more information.
    |2|CONFIG_FILE_NOTREADABLE|The configuration JSON is not readable. Make sure that the file exists and check the file permissions. It has nothing to do with the content of he file. If so you would see other error codes.