<div align="center">

# Configuration Helper

This document will help you to configure your VMs for the use with the Qemu manager. At first every mandatory configurations will be displayed. Then the optional properties will be shown. Furthermore we offer some example configurations for different purposes.

---

<p align="center">
<img alt="Logo banner" src="./images/penguin_books.png" style="width: 50%"/>

---

</div>

## General Structure

The configuration must always start with a "vms" root key which value is an array of vm configuration objects. The structure depends on the fields which can be simple value like strings or integers or objects or array of things. Concrete keys and values are written down in the example sections.

``` json
{
    "vms": [
        {
            "key1": "value1",
            "key2": [
                {
                    "key2": "value2",
                    "key3": 10,
                    ⋮
                },
                ⋮
            ],
            "key4": {
                "key5": "value3",
                "key6": "value4",
                ⋮
            }
        }
    ]
}
```

## Mandatory Fields

This section lists all mandatory fields for a valid configuration.

### Name (**string**)

Every vm config object needs a **unique** name.

``` json
"name": "example"
```

### RAM (**int**)

You must set a fix amount of memory (RAM, in GB) which should be used by the vm. The minimum is 1 (1GB). You can only input natural numbers => only 1GB steps.

``` json
"ram": 10
```

### CPU (**int**)

You must set a fix amount of virtual cores which should be used by the vm. The minimum is 1. You can only input natural numbers => only 1 core steps. The cores will be detect as virtual cores with one socket and one cpu in the guest.

``` json
"cpu": 4
```

**NOTE**: Sometimes MacOS only accept 2<sup>x</sup> core amount values. If you try to set f.e. 3 cores, MacOS will run into a strange boot error. We recommend at least 2 cores for stable use. But depending on your CPU you will detect big differences between 2 and 4 cores in the MacOS VM.

### OS (**string**)

You must set the OS type to make sure that OS specific configuration are correctly used.

``` json
"os": "linux"
```

Currently supported OSes:

- Linux: `"linux"`
- Windows: `"win"`
- Darwin/MacOS: `"macos"`

### Working Directory (**string**)

You must define an absolute path to a folder which contains things like:

- VMDK/QCOW/RAW images as virtual disks
- ISO files for booting OS installer or different things
- Custom scripts or other stuff

``` json
"workingdir": "/home/user/osx"
```

This is the only directory on the host the manager will have access to control your VM.

## Optional Fields

The following fields are not necessary to start the virtual machine but may extend the configuration in a way which make it much more usable.

### EFI (**bool**)

You can activate EFI boot for your VM. It will only use OVMF files which are statically embedded in this project.

``` json
"efi": true
```

**NOTE**: MacOS can only boot with special OVMF files which are also embedded into this project. But it means that legacy boot is not possible for MacOS. So if you set the OS to MacOS but not the EFI flag it will activate EFI though.

**Legacy**             |  **EFI**
:-------------------------:|:-------------------------:
![](./images/penguin_legacy.png)  |  ![](./images/penguin_magic.png)

### VNC (**int**)

You can add a VNC handler to the VM to get a virtual remote output (always recommended for the first installation). The value is a VNC port offset. If you define `0` it will use port `5900`.

`Port = 5900 + value`

``` json
"vnc": 1
```

will result in Port `5901`.

### Keyboard Layout (**string**)

You can choose a custom keyboard layout for the virtual keyboard which will be used f.e. within the VNC connection. The default is `en-US`.

``` json
"keyboard": "fr"
```

for a french keyboard layout. Here is an overview with all available country/language codes.

|Keyboard Layout Code|Meaning|
|:--:|:---:|
|ar|Arabic|
|de-ch|Switzerland|
|es|Spain|
|fo|Faroese|
|fr-ca|French - Canada|
|hu|Hungarian|
|ja|Japanese|
|mk|FYRO Macedonia|
|no|Norwegian|
|pt-br|Portuguese - Brazil|
|sv|Swedish|
|da|Danish|
|en-gb|Great Britain|
|et|Estonian|
|fr|France|
|fr-ch|French - Canada|
|is|Icelandic|
|lt|Lithuanian|
|nl|Dutch|
|pl|Polish|
|ru|Russian|
|th|Thai|
|de|German|
|en-us|English - United States|
|fi|Finnish|
|fr-be|French - Belgium|
|hr|Croatian|
|it|Italian|
|lv|Latvian|
|nl-be|Dutch - Belgium|
|pt|Portuguese|
|sl|Slovenian|
|tr|Turkish|

### Virtual CDROM (**array of strings**)

You can add one or multiple images (f.e. ISO files) as virtual drives. The image files **have to** be located in the configured working directory.

``` json
"cdrom": [
    "windows.iso"
]
```

*other option (multiple images)*:

``` json
"cdrom": [
    "windows.iso",
    "linux.img"
]
```

**NOTE**: If you enter this field the VM will boot from the first entered device in the list.

## Examples

TODO

### Example 1

TODO

``` json
{
    "vms": [
        {
            "name": "NilsDevMac",
            "ram": 15,
            "cpu": 4,
            "os": "macos",
            "workingdir": "/home/user/osx"
        },
        {
            "name": "NilsDevWin",
            "ram": 15,
            "cpu": 4,
            "os": "win",
            "workingdir": "/home/user/osx"
        },
        {
            "name": "NilsDevLinux",
            "ram": 15,
            "cpu": 4,
            "os": "linux",
            "efi": true,
            "workingdir": "/home/user/osx"
        }
    ]
}
```