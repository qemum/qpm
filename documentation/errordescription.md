<div align="center">

# Error Description

For making your troubleshooting as easy as possible this toolkit will return exact error codes for every known exception case.

---

<p align="center">
<img alt="Logo banner" src="./images/penguin_troubleshooting.png" style="width: 50%"/>

---

</div>

## QPM error codes

| Error Code | Stringified Name | Description |
|:----------:|:----------------:|:-----------:|
|0|Success|No error occurred|
|1|UnknownError|An unknown error occurred. Please check the inner error message for more information.
|2|ConfigFileNotReadable|The configuration JSON is not readable. Make sure that the file exists and check the file permissions. Perhaps the file is empty.
|3|ConfigMandatoryMissingName|The VM name could not be found in the configuration or it is empty.
|4|ConfigMandatoryInvalidRAM|The VM RAM amount was not specified or is an invalid value.
|5|ConfigMandatoryInvalidCPU|The VM CPU core amount was not specified or is an invalid value.
|6|ConfigMandatoryInvalidOS|You forgot to set an OS or the value is invalid.
|7|ConfigMandatoryInvalidWorkingDir|You forgot to set a working directory or the given path does not exist or the permissions aren't correctly set.
|8|ConfigInvalidConfigJSON|The config JSON seems to be malformed.
|9|ConfigInvalidDuplicateName|The config file contains different vms which have the same name. This is not allowed

## QPG error codes

TODO