// +build darwin

package main

func poweroff() []string {
	return []string{"shutdown", "-h", "now"}
}
