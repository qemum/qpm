package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	shared "gitlab.com/qemum/qpm/shared"
)

func main() {
	http.HandleFunc("/", handle)
	if err := http.ListenAndServe(":9999", nil); err != nil {
		log.Fatal(err)
	}
}

func handle(w http.ResponseWriter, req *http.Request) {
	decoder := json.NewDecoder(req.Body)
	t := shared.Guestcommand{}
	decoder.Decode(&t)

	result := ""
	switch t.Command {
	case "poweroff":
		shared.ExecCommand("", Poweroff())
	default:
		result = "Unknown command " + t.Command
	}

	responseData, err := json.Marshal(&shared.Guestresult{Result: result})
	if err != nil {
		log.Println(err)
	}
	fmt.Fprint(w, string(responseData))
}
