// +build windows

package main

func poweroff() []string {
	return []string{"shutdown", "/s", "/f", "/t", "0"}
}
