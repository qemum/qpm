package main

import (
	"errors"
	"strconv"
	"strings"
)

type osparam interface {
	CPU() string
	AdditionalCommandLines() string
}

type osx struct {
}

type win struct {
}

type linux struct {
}

// ConvertConfigToQemuCommand TODO
func ConvertConfigToQemuCommand(config *Vmconfig) ([]string, error) {
	cmd := []string{}
	// at first build the base command which will always be executed
	cmd = addParam(cmd, `qemu-system-x86_64`)
	// add parameters
	cmd = addParam(cmd, `-enable-kvm`)
	cmd = addParam(cmd, `-machine q35,accel=kvm`)
	cmd = addParam(cmd, `-name `+config.Name)
	cmd = addParam(cmd, `-smp `+strconv.Itoa(config.CPU)+`,cores=`+strconv.Itoa(config.CPU)+`,threads=1,sockets=1`)
	cmd = addParam(cmd, `-m `+strconv.Itoa(config.RAM)+`G`)
	cmd = addParam(cmd, `-smbios type=2`)
	cmd = addParam(cmd, `-nographic`)

	// check if VM is EFI VM and add the following command line if so
	if config.EFI {
		cmd = addParam(cmd, "-drive if=pflash,format=raw,readonly,file=firmware/OVMF_CODE.fd")
		cmd = addParam(cmd, "-drive if=pflash,format=raw,file=firmware/OVMF_VARS.fd")
	}

	// check if VNC value is set and handle it if so
	if config.VNC != "" {
		cmd = addParam(cmd, `-vnc :`+config.VNC)
		cmd = addParam(cmd, `-vga qxl`)
	}

	// check if keyboard code value is set and handle it if so
	if config.Keyboard != "" {
		cmd = addParam(cmd, `-k `+config.Keyboard)
	}

	// add all virtual cdrom drives if present in configuration
	if len(config.CDROM) > 0 {
		for _, image := range config.CDROM {
			cmd = addParam(cmd, `-drive file=`+image+`,media=cdrom`)
		}
	}

	// check which OS is selected and set a variable os with the chosen type
	var os osparam = (*win)(nil)
	switch config.OS {
	case "win":
		os = win{}
	case "macos":
		os = osx{}
	case "linux":
		os = linux{}
	default:
		return cmd, errors.New("OS " + config.OS + "is unknown")
	}

	// get specific CPU config
	cmd = addParam(cmd, os.CPU())
	// get specific OS additional command lines
	cmd = addParam(cmd, os.AdditionalCommandLines())
	return cmd, nil
}

// addParam TODO
func addParam(cmd []string, param string) []string {
	return append(cmd, strings.Fields(param)...)
}

// AdditionalCommandLines TODO
func (win) AdditionalCommandLines() string {
	return ""
}

// AdditionalCommandLines TODO
func (osx) AdditionalCommandLines() string {
	return `-device isa-applesmc,osk="ourhardworkbythesewordsguardedpleasedontsteal(c)AppleComputerInc"`
}

// AdditionalCommandLines TODO
func (linux) AdditionalCommandLines() string {
	return ""
}

// CPU TODO
func (win) CPU() string {
	return `-cpu host`
}

// CPU TODO
func (osx) CPU() string {
	return `-cpu Penryn,vendor=GenuineIntel,kvm=on,+invtsc,vmware-cpuid-freq=on,+avx,+aes,+xsave,+xsaveopt,enforce`
}

// CPU TODO
func (linux) CPU() string {
	return `-cpu host`
}
