package main

import (
	"html/template"
	"net/http"
	"path"
	"strconv"
	"time"

	"gitlab.com/qemum/qpm/shared"

	linq "github.com/ahmetb/go-linq"
)

const (
	// ConfigFilePath is currently the path to the configuration
	ConfigFilePath string = "config.json"
)

// UIData TODO
type UIData struct {
	Vminfos    []Vminfo
	Pcidevices []Pcidevice
}

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		vms, err := Listvms()
		if err != nil {
			handleError(err.Error(), w)
			return
		}

		devices, err := Listpcidevices()
		if err != nil {
			handleError(err.Error(), w)
			return
		}

		uiData := UIData{
			Vminfos:    vms,
			Pcidevices: devices,
		}

		// TODO constant for html path
		tmpl := template.Must(template.ParseFiles("./html/index.html"))
		tmpl.Execute(w, uiData)
	})
	http.Handle("/images/", http.StripPrefix("/images/", http.FileServer(http.Dir("./html/images"))))
	http.HandleFunc("/kill/", func(w http.ResponseWriter, r *http.Request) {
		// get vm list
		vms, err := Listvms()
		if err != nil {
			handleError(err.Error(), w)
			return
		}
		// get the chosen vmname
		vmname := path.Base(r.URL.Path)
		// get the corresponding vm info object
		vminfo := linq.From(vms).FirstWithT(func(vm Vminfo) bool {
			return vm.VM.Name == vmname
		}).(Vminfo)
		if vminfo.PID != 0 {
			_, err = shared.ExecCommand("", []string{"kill", "-9", strconv.Itoa(vminfo.PID)})
			if err != nil {
				handleError(err.Error(), w)
				return
			}
		}
		http.Redirect(w, r, "/", http.StatusSeeOther)
	})
	http.HandleFunc("/guest/", func(w http.ResponseWriter, r *http.Request) {
		// get the chosen command
		command := path.Base(r.URL.Path)
		SendCommand(command)
		http.Redirect(w, r, "/", http.StatusSeeOther)
	})
	http.HandleFunc("/vm/", func(w http.ResponseWriter, r *http.Request) {
		// get vm list
		vms, err := Listvms()
		if err != nil {
			handleError(err.Error(), w)
			return
		}
		// get the chosen vmname
		vmname := path.Base(r.URL.Path)
		// get the corresponding vm info object
		vminfo := linq.From(vms).FirstWithT(func(vm Vminfo) bool {
			return vm.VM.Name == vmname
		}).(Vminfo)

		res, _ := ConvertConfigToQemuCommand(&vminfo.VM)
		// Start qemu command in a new thread
		errorResponse := make(chan string, 1)
		go func() {
			_, err := shared.ExecCommand(vminfo.VM.Workingdir, res)
			if err != nil {
				errorResponse <- err.Error()
			}
		}()

		select {
		case errMessage := <-errorResponse:
			handleError(errMessage, w)
		// wait 5 seconds before showing the VM page
		case <-time.After(5000 * time.Millisecond):
			// read the vm template
			tmpl := template.Must(template.ParseFiles("./html/vm.html"))
			tmpl.Execute(w, vminfo)
		}
	})
	http.ListenAndServe(":8080", nil)
}

// handleError shows the HTML error page with further information from "errorMessage"
func handleError(errorMessage string, w http.ResponseWriter) {
	tmpl := template.Must(template.ParseFiles("./html/error.html"))
	tmpl.Execute(w, errorMessage)
}
