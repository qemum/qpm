package main

import (
	"encoding/json"
	"sync"
	"testing"

	"github.com/google/go-cmp/cmp"
)

type QpmconfigTest struct {
	Config    *Qpmconfig
	ErrorCode int
}

// Tests for the repository group and name getter
func TestRepoinfo(t *testing.T) {
	var testMatrix = []struct {
		value          string
		expectedResult *QpmconfigTest
	}{
		{
			"notexistingfile.json", &QpmconfigTest{
				ErrorCode: ConfigFileNotReadable,
			},
		},
		{
			"./testconfigs/emptyConfig.json", &QpmconfigTest{
				ErrorCode: ConfigFileNotReadable,
			},
		},
		{
			"./testconfigs/successTest1.json", &QpmconfigTest{
				Config: &Qpmconfig{
					VMS: []*Vmconfig{
						&Vmconfig{
							Name:       "Test1",
							OS:         "linux",
							RAM:        8,
							CPU:        4,
							Workingdir: ".",
						},
					},
				},
			},
		},
		{
			"./testconfigs/invalidNameConfig.json", &QpmconfigTest{
				ErrorCode: ConfigMandatoryMissingName,
			},
		},
		{
			"./testconfigs/invalidRamConfig.json", &QpmconfigTest{
				ErrorCode: ConfigMandatoryInvalidRAM,
			},
		},
		{
			"./testconfigs/invalidRamConfig2.json", &QpmconfigTest{
				ErrorCode: ConfigMandatoryInvalidRAM,
			},
		},
		{
			"./testconfigs/invalidRamConfig3.json", &QpmconfigTest{
				ErrorCode: ConfigMandatoryInvalidRAM,
			},
		},
		{
			"./testconfigs/invalidCpuConfig.json", &QpmconfigTest{
				ErrorCode: ConfigMandatoryInvalidCPU,
			},
		},
		{
			"./testconfigs/invalidCpuConfig2.json", &QpmconfigTest{
				ErrorCode: ConfigMandatoryInvalidCPU,
			},
		},
		{
			"./testconfigs/invalidOsConfig.json", &QpmconfigTest{
				ErrorCode: ConfigMandatoryInvalidOS,
			},
		},
		{
			"./testconfigs/invalidOsConfig2.json", &QpmconfigTest{
				ErrorCode: ConfigMandatoryInvalidOS,
			},
		},
		{
			"./testconfigs/invalidWorkingDir.json", &QpmconfigTest{
				ErrorCode: ConfigMandatoryInvalidWorkingDir,
			},
		},
		{
			"./testconfigs/invalidWorkingDir2.json", &QpmconfigTest{
				ErrorCode: ConfigMandatoryInvalidWorkingDir,
			},
		},
		{
			"./testconfigs/invalidJsonFile.json", &QpmconfigTest{
				ErrorCode: ConfigInvalidConfigJSON,
			},
		},
		{
			"./testconfigs/invalidDuplicateVmsConfig.json", &QpmconfigTest{
				ErrorCode: ConfigInvalidDuplicateName,
			},
		},
		{
			"./testconfigs/macosNonEfi.json", &QpmconfigTest{
				Config: &Qpmconfig{
					VMS: []*Vmconfig{
						&Vmconfig{
							Name:       "Test1",
							OS:         "macos",
							RAM:        8,
							CPU:        4,
							Workingdir: ".",
							EFI:        true,
						},
					},
				},
			},
		},
	}
	// define waiting group
	var wg sync.WaitGroup
	wg.Add(len(testMatrix))
	for i, test := range testMatrix {
		go func(i int, testFilePath string, expectedResult *QpmconfigTest) {
			defer wg.Done()
			readConfig, e := Readconfig(testFilePath)
			if e != nil {
				if expectedResult.ErrorCode == 0 {
					t.Errorf("Failed test: %d; file: %s; An error occurred but it was not expected: %d; %s", i, testFilePath, e.Code, e.InnerError)
				} else {
					if e.Code != expectedResult.ErrorCode {
						t.Errorf("Failed test: %d; file: %s; An error occurred that is expected but it is the false one: %d; %s; expected: %d", i, testFilePath, e.Code, e.InnerError, expectedResult.ErrorCode)
					} else {
						t.Logf("Successful test: %d; file: %s", i, testFilePath)
					}
				}
			} else {
				if expectedResult.ErrorCode != 0 {
					t.Errorf("Failed test: %d; file: %s; No error occurred but it was expected: %d (code)", i, testFilePath, expectedResult.ErrorCode)
				} else {
					if !cmp.Equal(expectedResult.Config, readConfig) {
						expectedStr, _ := json.Marshal(expectedResult.Config)
						readStr, _ := json.Marshal(readConfig)
						t.Errorf("Failed test: %d; file: %s; The expected config does not equal to the parsed one... expected: %s; read: %s", i, testFilePath, string(expectedStr), string(readStr))
					} else {
						t.Logf("Successful test: %d; file: %s", i, testFilePath)
					}
				}
			}
		}(i, test.value, test.expectedResult)
	}
	// Wait until all threads are finished
	wg.Wait()
}
