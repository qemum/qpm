package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	shared "gitlab.com/qemum/qpm/shared"
)

// SendCommand TODO
func SendCommand(command string) {
	jsonData, err := json.Marshal(&shared.Guestcommand{Command: command})
	if err != nil {
		log.Println(err)
	}
	resp, err := http.Post("http://127.0.0.1:37317/", "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	responseData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	res := shared.Guestresult{}
	json.Unmarshal(responseData, &res)
	fmt.Println(res)
}
