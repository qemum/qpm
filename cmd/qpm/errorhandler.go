package main

import "fmt"

const (
	// UnknownError is an unknown error.
	UnknownError = 1
	// ConfigFileNotReadable means that the config file does not exist or the permissions are not set correctly.
	ConfigFileNotReadable = 2
	// ConfigMandatoryMissingName you forgot to set a VM name
	ConfigMandatoryMissingName = 3
	// ConfigMandatoryInvalidRAM you forgot to set RAM or the value is invalid
	ConfigMandatoryInvalidRAM = 4
	// ConfigMandatoryInvalidCPU you forgot to set CPU cores or the value is invalid
	ConfigMandatoryInvalidCPU = 5
	// ConfigMandatoryInvalidOS you forgot to set an OS or the value is invalid
	ConfigMandatoryInvalidOS = 6
	// ConfigMandatoryInvalidWorkingDir you forgot to set a working directory or the given path does not exist or the permissions aren't correctly set
	ConfigMandatoryInvalidWorkingDir = 7
	// ConfigInvalidConfigJSON the config json seems to be malformed
	ConfigInvalidConfigJSON = 8
	// ConfigInvalidDuplicateName the config file contains different vms which have the same name. This is not allowed
	ConfigInvalidDuplicateName = 9
)

// Qpmerror TODO
type Qpmerror struct {
	Code       int
	InnerError string
}

func (err *Qpmerror) Error() string {
	switch err.Code {
	case ConfigFileNotReadable:
		return fmt.Sprintf("Could not find the config file path. Maybe the permissions are false or the file is empty %s", err.InnerError)
	case ConfigMandatoryMissingName:
		return fmt.Sprintf("You forgot to set a VM name")
	case ConfigMandatoryInvalidRAM:
		return fmt.Sprintf("You forgot to set RAM or the value is invalid")
	case ConfigMandatoryInvalidCPU:
		return fmt.Sprintf("You forgot to set CPU cores or the value is invalid")
	case ConfigMandatoryInvalidOS:
		return fmt.Sprintf("You forgot to set an OS or the value is invalid")
	case ConfigMandatoryInvalidWorkingDir:
		return fmt.Sprintf("You forgot to set a working directory or the given path does not exist or the permissions aren't correctly set")
	case ConfigInvalidConfigJSON:
		return fmt.Sprintf("The config JSON seems to be malformed")
	case ConfigInvalidDuplicateName:
		return fmt.Sprintf("The config file contains different vms which have the same name. This is not allowed")
	default:
		return fmt.Sprintf("Unknown error. %s", err.InnerError)
	}
}
