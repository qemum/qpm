package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/qemum/qpm/shared"

	linq "github.com/ahmetb/go-linq"
)

// Processinfo TODO
type Processinfo struct {
	PID  int
	Name string
}

// Listpcidevices TODO
func Listpcidevices() (devices []Pcidevice, err error) {
	// get all pci devices with its group
	out, err := shared.ExecCommand("", []string{"sh", "-c", `for g in /sys/kernel/iommu_groups/*; do for d in $g/devices/*; do echo -e "IOMMU Group ${g##*/}|$(lspci -s ${d##*/})|$(lspci -ns ${d##*/}))"; done; done`})
	if err != nil {
		return nil, err
	}
	groupkey := "Group"
	pciaddresskey := "Pciaddress"
	pcinamekey := "Pciname"
	pciidkey := "pciid"
	regexpattern := fmt.Sprintf(`IOMMU Group (?P<%s>\d{1,2})\|(?P<%s>\w\w:\w\w.\w)\s*(?P<%s>.*)\|.*(?P<%s>\w{4}:\w{4})`, groupkey, pciaddresskey, pcinamekey, pciidkey)
	entries := strings.Split(string(out), "\n")
	linq.From(entries).WhereT(func(entry string) bool {
		return isMatch(regexpattern, entry)
	}).SelectT(func(entry string) Pcidevice {
		regexresult := getParams(regexpattern, entry)
		group, _ := strconv.Atoi(regexresult[groupkey])
		device := Pcidevice{
			Address:    regexresult[pciaddresskey],
			Iommugroup: group,
			Name:       regexresult[pcinamekey],
			ID:         regexresult[pciidkey],
		}
		return device
	}).ToSlice(&devices)
	return devices, nil
}

// Listvms lists all vms (also the running one by comparing the vm config list)
func Listvms() (runningvms []Vminfo, e error) {
	// get vm config
	config, err := Readconfig(ConfigFilePath)
	if err != nil {
		return nil, err
	}
	// get all running processes
	processInfos, e := getProcinfo()
	if err != nil {
		return nil, err
	}
	// filter only for qemu processes
	linq.From(processInfos).WhereT(func(procinfo Processinfo) bool {
		return strings.HasPrefix(procinfo.Name, "qemu")
	}).ToSlice(&processInfos)
	// get all vm configs from the config which name are matching in any found process command
	linq.From((*config).VMS).SelectT(func(vm *Vmconfig) Vminfo {
		vminfo := Vminfo{VM: *vm}
		procinfo := linq.From(processInfos).FirstWithT(func(procinfo Processinfo) bool {
			return strings.Contains(procinfo.Name, "-name "+vm.Name)
		})
		if procinfo != nil {
			procinfo := procinfo.(Processinfo)
			vminfo.State = "running"
			vminfo.PID = procinfo.PID
		} else {
			vminfo.State = "halt"
			vminfo.PID = 0
		}
		return vminfo
	}).ToSlice(&runningvms)
	return runningvms, nil
}

// get all system processes with pid and argument command
func getProcinfo() (processInfos []Processinfo, err error) {
	// get all current running processes
	out, err := shared.ExecCommand("", []string{"sh", "-c", `ps -Ao "%p|%a"`})
	if err != nil {
		return nil, err
	}
	splitProcess := func(process string) []string {
		return strings.Split(process, "|")
	}
	// get all processes which command are starting with "qemu"
	processes := strings.Split(string(out), "\n")
	linq.From(processes).WhereT(func(process string) bool {
		return len(splitProcess(process)) == 2
	}).SelectT(func(process string) Processinfo {
		processinfo := splitProcess(process)
		pid, _ := strconv.Atoi(strings.TrimSpace(processinfo[0]))
		return Processinfo{PID: pid, Name: processinfo[1]}
	}).ToSlice(&processInfos)
	return processInfos, nil
}

// getParams gets grouped results from a regex string and its corresponding input string.
func getParams(pattern, input string) (paramsMap map[string]string) {
	var compRegEx = regexp.MustCompile(pattern)
	match := compRegEx.FindStringSubmatch(input)

	paramsMap = make(map[string]string)
	for i, name := range compRegEx.SubexpNames() {
		if i > 0 && i <= len(match) {
			paramsMap[name] = match[i]
		}
	}
	return
}

// isMatch checks if the given Regex pattern matches to he input string.
func isMatch(pattern, input string) bool {
	match, err := regexp.MatchString(pattern, input)
	if err != nil {
		return false
	}
	return match
}
