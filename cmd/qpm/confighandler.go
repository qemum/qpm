package main

import (
	"encoding/json"
	"io/ioutil"
	"os"

	linq "github.com/ahmetb/go-linq"
	"github.com/google/go-cmp/cmp"
)

// Qpmconfig TODO
type Qpmconfig struct {
	VMS []*Vmconfig `json:"vms"`
}

// Pcidevice TODO
type Pcidevice struct {
	Address    string `json:"address"`
	Iommugroup int    `json:"iommugroup"`
	Name       string `json:"name"`
	ID         string `json:"id"`
}

// Vmconfig TODO
type Vmconfig struct {
	Name       string   `json:"name"`
	RAM        int      `json:"ram"`
	CPU        int      `json:"cpu"`
	OS         string   `json:"os"`
	EFI        bool     `json:"efi"`
	Workingdir string   `json:"workingdir"`
	VNC        string   `json:"vnc"`
	Keyboard   string   `json:"keyboard"`
	CDROM      []string `json:"cdrom"`
}

// Vminfo TODO
type Vminfo struct {
	VM    Vmconfig `json:"vm"`
	PID   int      `json:"pid"`
	State string   `json:"state"`
}

// Readconfig reads the vm configuration into the corresponding struct
func Readconfig(configPath string) (*Qpmconfig, *Qpmerror) {
	// Read the config file into a byte array.
	config, err := ioutil.ReadFile(configPath)
	if err != nil {
		return nil, &Qpmerror{Code: ConfigFileNotReadable, InnerError: err.Error()}
	}
	// If the file is empty, raise an error
	if string(config) == "" {
		return nil, &Qpmerror{Code: ConfigFileNotReadable}
	}
	res := Qpmconfig{}
	// Try to parse the JSON file
	err = json.Unmarshal(config, &res)
	// If the parsed config is not the default object try to continue (partly successful parsed)
	if err != nil && cmp.Equal(res, Qpmconfig{}) {
		return nil, &Qpmerror{Code: ConfigInvalidConfigJSON, InnerError: err.Error()}
	}
	return &res, validateconfig(&res)
}

// validates the read config file
func validateconfig(config *Qpmconfig) *Qpmerror {
	configVmsAmount := len(config.VMS)
	if configVmsAmount > 0 {
		// Check for invalid config things like duplicate keys in one object or something like that
		if err := checkInvalidConfigFields(config); err != nil {
			return err
		}
		// For loop over all config files
		for _, vm := range config.VMS {
			if err := checkMandatoryFields(vm); err != nil {
				return err
			}
			if err := checkOptionalFields(vm); err != nil {
				return err
			}
		}
	}
	return nil
}

func checkInvalidConfigFields(config *Qpmconfig) *Qpmerror {
	// Detect if different VM configs use the same name
	duplicates := linq.From(config.VMS).SelectT(func(config *Vmconfig) string {
		return config.Name
	}).GroupByT(func(name string) string {
		return name
	}, func(name string) string {
		return name
	}).CountWithT(func(group linq.Group) bool {
		return len(group.Group) > 1
	})
	if duplicates > 0 {
		return &Qpmerror{Code: ConfigInvalidDuplicateName}
	}
	// No error occurred
	return nil
}

// checkMandatoryFields checks if the mandatory fields of the configuration are valid
func checkMandatoryFields(vm *Vmconfig) *Qpmerror {
	// Check if all mandatory fields are available
	if vm.Name == "" {
		return &Qpmerror{Code: ConfigMandatoryMissingName}
	}
	if vm.RAM <= 0 {
		return &Qpmerror{Code: ConfigMandatoryInvalidRAM}
	}
	if vm.CPU <= 0 {
		return &Qpmerror{Code: ConfigMandatoryInvalidCPU}
	}
	if vm.OS != "" {
		if vm.OS != "linux" && vm.OS != "win" && vm.OS != "macos" {
			return &Qpmerror{Code: ConfigMandatoryInvalidOS}
		}
	} else {
		return &Qpmerror{Code: ConfigMandatoryInvalidOS}
	}
	if vm.Workingdir != "" {
		if _, err := os.Stat(vm.Workingdir); os.IsNotExist(err) {
			return &Qpmerror{Code: ConfigMandatoryInvalidWorkingDir}
		}
	} else {
		return &Qpmerror{Code: ConfigMandatoryInvalidWorkingDir}
	}
	// No error occurred
	return nil
}

// checkOptionalFields checks if the optional fields of the configuration are valid
func checkOptionalFields(vm *Vmconfig) *Qpmerror {
	// if OS is MacOS EFI must be used so it will be set
	if vm.OS == "macos" {
		vm.EFI = true
	}
	// No error occurred
	return nil
}
