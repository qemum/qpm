package shared

import (
	"bytes"
	"errors"
	"os/exec"
)

// ExecCommand TODO
func ExecCommand(path string, command []string) (string, error) {
	cmd := exec.Command(command[0], command[1:]...)
	cmd.Dir = path
	cmdOutput := &bytes.Buffer{}
	cmdError := &bytes.Buffer{}
	cmd.Stdout = cmdOutput
	cmd.Stderr = cmdError
	err := cmd.Run()
	if err != nil {
		return "", errors.New(cmdError.String())
	}
	return cmdOutput.String(), nil
}
