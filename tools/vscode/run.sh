#!/bin/bash
if [[ $EUID -eq 0 ]]; then
  echo "This script must NOT be run as root" 1>&2
  exit 1
fi

PROJECT_PATH="$( realpath ../.. )"
CONTAINER_USER="developer"
CONTAINER_NAME="registry.gitlab.com/qemum/qpm/vscode"

docker run -d --rm \
-e DISPLAY \
-v $HOME/.Xauthority:/home/$CONTAINER_USER/.Xauthority \
-v $PROJECT_PATH:/go/src/gitlab.com/qemum/qpm \
--net=host \
--name vscode \
$CONTAINER_NAME
