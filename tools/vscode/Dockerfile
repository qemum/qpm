FROM golang:1.13.3-buster

ENV DEBIAN_FRONTEND noninteractive
ENV GROUP=users
ENV USER=developer
ENV ZSH_THEME agnoster

RUN apt-get update \
 && apt-get install -y zsh fonts-powerline curl apt-transport-https libgtk2.0-0 libxss1 libasound2 xauth x11-apps dbus git gpg gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget

RUN useradd -m -G $GROUP $USER --shell /bin/zsh

RUN mkdir /var/run/dbus
RUN curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg \
 && mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg \
 && sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'

RUN apt-get update \
 && apt-get install -y code \
 && apt-get install -f

RUN cp /usr/lib/x86_64-linux-gnu/libxcb.so.1 /usr/share/code/ \
 && cp /usr/lib/x86_64-linux-gnu/libxcb.so.1.1.0 /usr/share/code/ \
 && sed -i 's/BIG-REQUESTS/_IG-REQUESTS/' /usr/share/code/libxcb.so.1 \
 && sed -i 's/BIG-REQUESTS/_IG-REQUESTS/' /usr/share/code/libxcb.so.1.1.0

USER $USER

RUN wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh || true

RUN /usr/bin/code --install-extension ms-vscode.Go \
  && go get -u -v github.com/ramya-rao-a/go-outline \
  && go get -u -v github.com/sqs/goreturns \
  && go get -u -v github.com/rogpeppe/godef \
  && go get -u -v github.com/uudashr/gopkgs/cmd/gopkgs \
  && go get -u -v github.com/ramya-rao-a/go-outline \
  && go get -u -v github.com/stamblerre/gocode \
  && go get -u -v github.com/mdempsky/gocode \
  && go get -u -v github.com/rogpeppe/godef \
  && go get -u -v golang.org/x/lint/golint \
  && go get -u -v golang.org/x/tools/gopls


RUN cd /go/src/github.com/stamblerre/gocode && go build -v -o /go/bin/gocode-gomod && cd /go/src/golang.org/x/tools/gopls && go build -v -o /go/bin/gopls

USER root
COPY settings.json /home/$USER/.config/Code/User
RUN chown -R $USER:$GROUP /home/$USER/.config/Code/User && chown -R $USER:$GROUP /go

USER $USER

ENTRYPOINT go get -v -t /go/src/gitlab.com/qemum/qpm/... && /usr/bin/code /go/src/gitlab.com/qemum/qpm --verbose
