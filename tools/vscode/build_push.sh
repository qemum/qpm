#!/bin/sh

PROJECT_PATH=$( realpath ../.. )
CONTAINER_NAME="registry.gitlab.com/qemum/qpm/vscode"

docker build -t $CONTAINER_NAME .
docker push $CONTAINER_NAME